package ex1;

import java.util.Comparator;

public class Persona implements Comparable<Persona> {
	private int pes; // en kg
	private int edat; // en anys
	private int alcada; // en cm
	
	public Persona() {
		edat = 20;
		pes = 70;
		alcada = 170;
	}
	
	public Persona(int edat, int pes, int alcada) throws IllegalArgumentException {
		setEdat(edat);
		setPes(pes);
		setAlcada(alcada);
	}
	
	public int getPes() {
		return pes;
	}

	public int getEdat() {
		return edat;
	}

	public int getAlcada() {
		return alcada;
	}

	public void setPes(int pes) throws IllegalArgumentException {
		if (pes <= 0)
			throw new IllegalArgumentException("El pes ha de ser positiu");
		this.pes = pes;
	}

	public void setEdat(int edat) throws IllegalArgumentException {
		if (edat < 0)
			throw new IllegalArgumentException("L'edat ha de ser positiva");
		this.edat = edat;
	}

	public void setAlcada(int alcada) throws IllegalArgumentException {
		if (alcada <= 0)
			throw new IllegalArgumentException("L'alçada ha de ser positiva");
		this.alcada = alcada;
	}

	@Override
	public String toString() {
		return "Persona [pes=" + getPes() + ", edat=" + getEdat() + ", alcada=" + getAlcada()
				+ "]";
	}

	@Override
	public int compareTo(Persona p) {
		return getAlcada() - p.getAlcada();
	}

	/*
	 * Aquest Comparator utilitza l'estil de Java 7 o inferior
	 */
	public static final Comparator<Persona> COMPARA_EDAT = 
			new Comparator<Persona>() {
		@Override
		public int compare(Persona p1, Persona p2) {
			return p1.getEdat() - p2.getEdat();
		}
	};
	/*
	 * Aquest Comparator utilitza una funció lambda
	 */
	public static final Comparator<Persona> COMPARA_PES = 
			(p1, p2) -> p1.getPes() - p2.getPes(); 
}
