
public class Persona {
	private int pes;
	private int edat;
	private int alcada;
	
	public int getPes() {
		return pes;
	}
	public void setPes(int pes) {
		this.pes = pes;
	}
	public int getEdat() {
		return edat;
	}
	public void setEdat(int edat) {
		this.edat = edat;
	}
	public int getAlcada() {
		return alcada;
	}
	public void setAlcada(int alcada) {
		this.alcada = alcada;
	}
	
	 public int compareTo(Persona arg0) {
	       return getAlcada() - arg0.getAlcada();
	   }
	public static void main(String[] args){
		Persona persona1 = new Persona();
		Persona persona2 = new Persona();
		int resultado = 0;
		
		persona1.setAlcada(171);
		persona2.setAlcada(178);
		
		if(persona1.compareTo(persona2)<0){
			System.out.print("La m�s alta es la persona 2.");
		}
		else if(persona1.compareTo(persona2)==0){
			System.out.print("Las 2 personas tienen la misma altura.");
		}
		else {//(persona1.compareTo(persona2)>0){
			System.out.print("La m�s alta es la persona 2.");
		}
		
	}
}

