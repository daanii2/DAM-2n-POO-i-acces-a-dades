package ex2;

import java.util.Comparator;


public class Persona{
	int pes;
	int edat;
	int alcada;
	
	public Persona (int pes, int edat, int alcada){
		this.pes = pes;
		this.edat = edat;
		this.alcada = alcada;
		
	}
	public int getPes() {
		return pes;
	}
	public void setPes(int pes) {
		this.pes = pes;
	}
	public int getEdat() {
		return edat;
	}
	public void setEdat(int edat) {
		this.edat = edat;
	}
	public int getAlcada() {
		return alcada;
	}
	public void setAlcada(int alcada) {
		this.alcada = alcada;
	}
	
	@Override
	public String toString() {
		return "Persona [pes=" + getPes() + ", edat=" + getEdat() + ", alcada=" + getAlcada()
				+ "]";
	}
	
	public static final Comparator<Persona> COMPARA_EDAT = 
			new Comparator<Persona>() {
		@Override
		public int compare(Persona p1, Persona p2) {
			return p1.getEdat() - p2.getEdat();
		}
	};
	
	public static final Comparator<Persona> COMPARA_PES = 
			(p1, p2) -> p1.getPes() - p2.getPes(); 
		 
	public static void main (String [ ] args) {
	int result;
	Persona p1, p2;
	p1 = new Persona(70,23,171);
	p2 = new Persona(80,22,160);
	result = Persona.COMPARA_EDAT.compare(p1, p2);
	if (result < 0)
		System.out.println("La primera mas joven que la segunda.");
	else if (result > 0)
		System.out.println("La primera es mas mayor que la segunda.");
	else 
		System.out.println("Las dos personas tienen la misma edad.");
	
	result = Persona.COMPARA_PES.compare(p1, p2);
	if (result < 0)
		System.out.println("La primera es mas delgada que la segunda.");
	else if (result > 0)
		System.out.println("La primera pesa mas que la segunda.");
	else 
		System.out.println("Las dos personas pesan lo mismo.");
    
	
	}
	      
}
