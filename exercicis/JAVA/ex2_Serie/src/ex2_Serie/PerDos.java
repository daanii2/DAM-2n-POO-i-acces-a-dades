package ex2_Serie;

public class PerDos implements Serie {
	private double actual;

	public PerDos() {
		inicialitza();
	}
	
	public PerDos(double llavor) {
		inicialitza(llavor);
	}
	
	@Override
	public void inicialitza() {
		actual = 1;
	}

	@Override
	public void inicialitza(double llavor) {
		actual = llavor;
	}

	@Override
	public double seguent() {
		actual *= 2;
		return actual;
	}
}
