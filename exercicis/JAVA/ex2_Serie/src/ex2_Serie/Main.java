package ex2_Serie;

public class Main {
	public static void main(String[] args) {
		Serie s1 = new PerDos();
		Serie s2 = new NegEntreDos();
		
		primersNombres(s1, 5);
		primersNombres(s2, 2);
	}

	public static void primersNombres(Serie s, int llavor) {
		s.inicialitza(llavor);
		
		for (int i=0; i<20; i++)
			System.out.println(i+": "+s.seguent());

}}
