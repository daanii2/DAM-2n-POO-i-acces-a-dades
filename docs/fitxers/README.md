* [Interfície *Path*](path.md)
* [Lectura/escriptura byte a byte](fitxers_bytes.md)
* [Lectura/escriptura de dades binàries](fitxers_dades.md)
* [Lectura/escriptura d'objectes](fitxers_objectes.md)
