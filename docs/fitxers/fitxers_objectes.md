## Lectura i escriptura d'objectes a fitxers binaris

A l'apartat anterior hem vist com podem crear fitxers binaris guardant-hi
dades simples. Sovint però voldrem guardar objectes sencers a fitxers, de
manera que sigui senzill recuperar després aquests objectes en un futur.

Java ens facilita la feina de guardar i recuperar objectes amb les classes
*ObjectOutputStream* i *ObjectInputStream*.

Per tal que els objectes d'una classe es puguin guardar i carregar
utilitzant aquest sistema cal que implementin la interfície **Serializable**.
Aquesta interfície no té cap mètode, serveix només per marcar que els
objectes d'aquella classe es poden guardar en fitxers.

En aquest apartat treballarem amb aquesta senzilla, que l'única novetat que
presenta és que implementa *Serializable*:

```java
public class Mascota implements Serializable {
	private static final long serialVersionUID = 1L;

	private String nom;
	private int numPotes;
	private boolean pel = true;

	public Mascota(String nom, int numPotes) {
		this.nom=nom;
		this.numPotes=numPotes;
	}

	public Mascota(String nom, int numPotes, boolean pel) {
		this(nom,numPotes);
		this.pel=pel;
	}

	public String getNom() {
		return nom;
	}

	public int getNumPotes() {
		return numPotes;
	}

	public boolean hasPel() {
		return pel;
	}
}
```

### Escriptura d'objectes

En el següent exemple creem una colla d'objectes *Mascota* i els guardem
en un fitxer:

```java
public class EscriptorObjectes {

	public static void main(String[] args) {
		Mascota[] mascotes = new Mascota[4];
		mascotes[0] = new Mascota("Rudy", 4);
		mascotes[1] = new Mascota("Piolin", 2, false);
		mascotes[2] = new Mascota("Nemo", 0, false);
		mascotes[3] = new Mascota("Tara", 8);

		try (ObjectOutputStream escriptor = new ObjectOutputStream(new FileOutputStream("prova.bin"));) {
			for (Mascota m : mascotes) {
				escriptor.writeObject(m);
			}
		} catch (IOException ex) {
			System.err.println(ex);
		}
	}
}
```
