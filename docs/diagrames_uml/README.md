**POO i accés a dades**

M5UF3. Introducció al disseny orientat a objectes
=================================================

Unified Modeling Language (UML)
-------------------------------

L'UML és un llenguatge de modelat que intenta abarcar totes les fases d'anàlisi,
disseny i desenvolupament d'una aplicació, especialment en els casos orientats
a objectes.

L'UML utilitza una sèrie de diagrames de diversos tipus que ofereixen
una gran flexibilitat. L'última versió de l'UML és la 2.5, de juny de 2015.

Hi ha dos grans grups de diagrames, els estructurals, que representen aspectes
estàtics, i els de comportament, que representen relacions dinàmiques, com
interaccions o comunicació entre elements. En total hi ha 7 tipus de diagrames
estructurals i 7 tipus més de diagrames de comportament.

Aquí veurem només els diagrames més utilitzats a la pràctica.

 * [Diagrames de classes](diagrames_classes/README.md)
 * [Diagrames de seqüència](diagrames_sequencia/README.md)
 * [Casos d'ús](casos_dus/README.md)
