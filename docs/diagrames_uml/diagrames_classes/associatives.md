## Classes associatives

Les **classes associatives** ens permeten assignar propietats i operacions
a la relació entre dues classes.

Imaginem per exemple que de cada hora de classe volem guardar a quin
ordinador s'ha assegut cadascun dels alumnes.

Hi ha dues maneres d'indicar això:

1. Com una *classe associativa*:

![Classe associativa](docs/diagrames_uml/imatges/exemple_classe_associativa.png)

2. Com una classe comuna:

![Classe comuna](docs/diagrames_uml/imatges/exemple_classe_associativa_001.png)

La diferència aquí és subtil però important: indicant que *Posicio* és una
classe associativa estem deixant clar que només hi pot haver una *Posicio*
per a cada parella d'alumne-classe.

En canvi, en el segon cas, res impedeix que creem dues posicions diferents
per al mateix alumne a la mateixa hora de classe.

Així, una classe associativa afegeix una restricció que fa referència
a la relació entre les dues classes que enllaça.

Imaginem ara que modelem així la relació entre un professor i les matèries
que imparteix a un grup:

![Falsa classe associativa](docs/diagrames_uml/imatges/exemple_classe_associativa_002.png)

Fixeu-vos que segons aquest diagrama no és possible que un professor
imparteixi dues matèries diferents al mateix grup. Si aquesta és una
possibilitat en el nostre model, la classe *Imparticio* no hauria de
ser una classe associativa.

La implementació d'una classe associativa es pot fer simplement com si
fos una classe habitual, amb mètodes que permetin accedir als objectes
associats.