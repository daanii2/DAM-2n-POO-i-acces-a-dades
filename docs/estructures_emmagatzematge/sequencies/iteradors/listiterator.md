##### Els iteradors de llistes: *ListIterator*

Fixeu-vos que podem utilitzar iteradors també sobre conjunts. Encara que
en un conjunt no es pugui garantir l'ordre dels seus elements, res
impedeix que els puguem recórrer tots. En el cas de les llistes, com que
a més tenen ordre, existeix un tipus especialitzat d'iterador, el
*ListIterator*, i el mètode *listIterator()* de la interfície *List* per
obtenir-ne un exemplar. Amb un *ListIterator*, a més de recórrer els
elements des del primer fins a l'últim, ho podem fer al revés, movent
l'iterador en el sentit que ens interessi i inicialitzant-lo a la
posició que vulguem.

Un *ListIterator* té tots els mètodes d'un *Iterator* (la interfície
*ListIterator* estén la *Iterator*), i afegeix els mètodes *previous()*
i *hasPrevious()* anàlegs a *next()* i *hasNext()*, però però recórrer
la llista en sentit contrari. A més, el mètode *listIterator* pot rebre
un enter, tal que la primer crida a *next* serà l'element que ocupa la
posició indicada per aquest enter.

Ens hem d'imaginar que un iterador d'aquest tipus es troba entremig de
dos elements, que una crida a *next* retornarà el de la dreta, i una
crida a *previous* retornarà el de l'esquerra:

![ListIterator](../../imatges/listiterator.png)

El següent codi recorreria una llista començant per l'últim element i
acabant pel primer:

```java
Integer i;
ListIterator<Integer> it = l.listIterator(l.size());
while (it.hasPrevious()) {
    i = it.previous();
    // fer alguna cosa amb i
}
```
